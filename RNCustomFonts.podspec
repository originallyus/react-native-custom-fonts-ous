
Pod::Spec.new do |s|
  s.name         = "RNCustomFonts"
  s.version      = "0.1.3"
  s.summary      = "RNCustomFonts"
  s.description  = <<-DESC
                  Use fonts sourced from a network address.
                   DESC
  s.homepage     = "https://gitlab.com/originallyus/react-native-custom-fonts-ous.git"
  s.license      = "MIT"
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://gitlab.com/originallyus/react-native-custom-fonts-ous.git", :tag => s.version.to_s }
  s.source_files  = "**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  
